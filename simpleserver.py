import sys
import os
import mimetypes
from wsgiref import simple_server, util
import cgi
import cgitb; cgitb.enable()
from string import Template

html = """
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>SimpleServer</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<html>
    <body>

        $body

    </body>
</html>
"""

template = Template(html)

def CreateDir():
    Static_dir = os.path.join(os.getcwd(), "static")
    if not os.path.isdir(Static_dir):
        try:
            os.mkdir(Static_dir)
        except OSError as error:
            print(error)

CreateDir()

def app(environ, respond):

    static_dir = os.path.join(path, 'static')

    fn = os.path.join(static_dir, environ['PATH_INFO'][1:])

    if '.' not in fn.split(os.path.sep)[-1]:

        if environ['PATH_INFO'] == "/":

            IndexPath = os.path.join(os.getcwd(), "static",'index.html')

            if not os.path.exists(IndexPath):

                links = '<div>Contenu du dossier :</div>\n'

                if len(os.listdir(static_dir)) == 0:


                    output = template.substitute({'body' : str(links) })


                    respond('200 OK', [('Content-Type', 'text/html'),('Content-Length', str(len(output)))])
                    return [output.encode('utf8')]

                else:

                    for fileName in os.listdir(static_dir) :


                        links += '<a href="/' + fileName + '" target="_blank" >' + fileName + "</a><br>\n"


                        output = template.substitute({'body' : str(links) })


                        respond('200 OK', [('Content-Type', 'text/html'),('Content-Length', str(len(output)))])
                        return [output.encode('utf8')]




            else:
                fn = os.path.join(fn, 'index.html')

        for fileName in os.listdir(static_dir) :

            if environ['PATH_INFO'] == "/" + fileName:

                fn = os.path.join(fn, fileName)

        if environ['PATH_INFO'] == "/form":

            if environ['REQUEST_METHOD'] == 'POST':

                fields = cgi.FieldStorage(fp=environ['wsgi.input'],environ=environ, keep_blank_values=True)

                Submited = """
                <a href="/">retour</a>
                <div>réponses transmises</div>
                """

                for key in fields.keys():

                    value = str(fields.getvalue(str(key)))
                    Submited += "<div>"+ key + "=" + value +"</div>\n"



                output = template.substitute({'body' : str(Submited) })


                respond('200 OK', [('Content-Type', 'text/html'),('Content-Length', str(len(output)))])
                return [output.encode('utf8')]


    type = mimetypes.guess_type(fn)[0]

    if os.path.exists(fn):
        respond('200 OK', [('Content-Type', type)])
        return util.FileWrapper(open(fn, "rb"))
    else:
        respond('404 Not Found', [('Content-Type', 'text/plain')])
        return [b'not found']

   
if __name__ == '__main__':
    path = sys.argv[1] if len(sys.argv) > 1 else os.getcwd()
    port = int(sys.argv[2]) if len(sys.argv) > 2 else 8000
    httpd = simple_server.make_server('127.0.0.1', port, app)
    url = 'http://localhost:' + str(port)
    print(" Le serveur python fonctionne sur le port : {} \n Aperçu disponible à l'adresse suivante : {} \n Pour interrompre le processus presser : ctrl + C ".format(port, url))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print('Processus interrompu')
        httpd.server_close()
        
        
